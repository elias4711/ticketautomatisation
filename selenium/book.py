from os import name
from selenium import webdriver
from selenium.webdriver.support.select import Select
import time
import json


# some JSON:
Tickets = '[{ "name":"Frank", "lastname":"Henin", "street":"An der Wilden Gutach 4", "zipcode":"79261", "city":"Gutach"}, { "name":"Ella", "lastname":"Henin", "street":"An der Wilden Gutach 4", "zipcode":"79261", "city":"Gutach"}, { "name":"Ingrid", "lastname":"Henin", "street":"An der Wilden Gutach 4", "zipcode":"79261", "city":"Gutach"}]'

Personen = json.loads(Tickets)
Ticketanzahl = len(Personen)

driver3 = webdriver.Chrome('/Users/User/Documents/ticketautomatisation/selenium/chromedriver.exe')
driver3.get('https://eveeno.com/schwimmbad')
time.sleep(3)

available = driver3.find_element_by_class_name('available')
available.click()

time.sleep(2)

emailField = driver3.find_element_by_id('email')
emailField.send_keys('eliasenglen2@gmail.com')

terms = driver3.find_element_by_id('terms1')
terms.click()

tickets = Select(driver3.find_element_by_class_name("shopCatAmount"))
tickets.select_by_index(Ticketanzahl)

weiterBtn = driver3.find_element_by_id('btn-checkoutShop1')
weiterBtn.click()

time.sleep(3)

for x in range(0, Ticketanzahl):
    firstnameid = 'firstname-' + str(x)
    firstname = driver3.find_element_by_id(firstnameid)
    firstname.send_keys(Personen[x]["name"])
    
    lastnameid = 'lastname-' + str(x)
    lastname = driver3.find_element_by_id(lastnameid)
    lastname.send_keys(Personen[x]["lastname"])

    streetid = 'street-' + str(x)
    street = driver3.find_element_by_id(streetid)
    street.send_keys(Personen[x]["street"])

    zipcodeid = 'zipcode-' + str(x)
    zipcode = driver3.find_element_by_id(zipcodeid)
    zipcode.send_keys(Personen[x]["zipcode"])

    cityid = 'city-' + str(x)
    City = driver3.find_element_by_id(cityid)
    City.send_keys(Personen[x]["city"])


weiterBtnzwei = driver3.find_element_by_id('btn-checkoutShop2')
weiterBtnzwei.click()

time.sleep(3)

driver3.close()